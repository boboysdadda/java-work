package com.boboysdadda.MethodSuiteTester;


import com.boboysdadda.corelib.MethodSuite;

/**
 * Project:     MethodSuiteTester
 * FileName:    MethodSuiteTester.java
 * Author:      james
 * Date:        3/31/2016
 * Description:
 */
class MethodSuiteTester {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {

        //Test Variables
        double[] numbers = {2.5,3.4,5.1};
        int[] ints = {1,2,3,4,5,6,7,8,9,10};
        MethodSuite tester = new MethodSuite();

        //Test Arguments
        System.out.println("Expect: 6. Got: " + tester.max(5,6));
        System.out.println("Expect: 6. Got: " + tester.max(6,5));
        System.out.println("Expect 11.0. Got: " + tester.total(numbers));
        System.out.println("Expect 10. Got: " + tester.max(ints));
        System.out.println("Expect 3.6. Got: " + tester.average(numbers));
    }
}
