package com.boboysdadda.calculateaverage;

import com.boboysdadda.corelib.FileWorker;
import com.boboysdadda.corelib.MethodSuite;

import java.io.IOException;

/**
 * Project: Calculate Average
 * FileName: CalculateAverage.java
 * Author: James Dreier
 * Date: 31 mar 2016
 * Description: Get or create a file called grades and then check that file for integers. Take those Integers and
 * put them in an array and get the average of that array.
 */
class CalculateAverage {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     * @throws IOException the io exception
     */
    public static void main(String[] args) throws IOException {

        // The filename to be worked with
        String fileName = "grades";

        //Creates objects to work with
        FileWorker grades = new FileWorker();
        MethodSuite numberWorker = new MethodSuite();

        //Gets or creates a file based on the filename given
        grades.getTextFile(fileName);

        //Creates an array from the file
        int[] gradesArray = grades.castTxtFileToIntArray(grades.getTextFile(fileName));

        //outputs the average
        System.out.println("The Average Grade is: " + numberWorker.average(gradesArray));

    }
}
