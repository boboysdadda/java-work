package com.boboysdadda.rpggame;

/**
 * Program:     com.boboysdadda.rpggame
 * File Name:   CharacterChoice.java
 * Author:      james
 * Date Created:4/23/2016
 * Description: A Class that handles Selecting the type of character the player chooses. and
 * creates an object of that Character type.
 */
public class CharacterChoice {

    public Character character(int choice){
        Character character;
        switch (choice){

            case 1:
                 character = new Warrior();
                return character;

            default:
                character = new Warrior();
        }

        return character;
    }
}
