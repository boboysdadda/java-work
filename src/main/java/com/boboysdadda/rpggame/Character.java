package com.boboysdadda.rpggame;

/**
 * Program:     com.boboysdadda.rpggame
 * File Name:   Character.java
 * Author:      james
 * Date Created:4/23/2016
 * Description: A class to handle characters
 */
public abstract class Character {

    private int strength;
    private int stamina;
    private int defense;
    private int agility;
    private int intelligence;
    private int willpower;
    private int level;
    private String name;
    private int[] attributes = new int[6];

    /**
     * @param attribute 0 = strength, 1 = stamina, 2 = defense, 3 = agility, 4 = intelligence 5 =
     *                  willpower
     * @param value     The amount you want the attribute increased.
     */

    public void addAttribute(int attribute, int value) {
        if (attribute <= 5 && attribute >= 0) {
            attributes[attribute] += value;
            switch (attribute) {
                case 0:
                    setStrength(attributes[attribute] + getStrength());
                    break;
                case 1:
                    setStamina(attributes[attribute] + getStamina());
                    break;
                case 2:
                    setDefense(attributes[attribute] + getDefense());
                    break;
                case 3:
                    setAgility(attributes[attribute] + getAgility());
                    break;
                case 4:
                    setIntelligence(attributes[attribute] + getIntelligence());
                    break;
                case 5:
                    setWillpower(attributes[attribute] + getWillpower());
                    break;
            }
        }
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getStamina() {
        return stamina;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getAgility() {
        return agility;
    }

    public void setAgility(int agility) {
        this.agility = agility;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getWillpower() {
        return willpower;
    }

    public void setWillpower(int willpower) {
        this.willpower = willpower;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
    public String getAttributes() {
        return "strength=" + getStrength() + "\n" +
                "stamina=" + getStamina() + "\n" +
                "defense=" + getDefense() + "\n" +
                "agility=" + getAgility() + "\n" +
                "intelligence=" + getIntelligence() + "\n" +
                "willpower=" + getWillpower();
    }

    public abstract String characterClass();

    @Override
    public String toString() {
        return "Character{" +
                "strength=" + strength +
                ", stamina=" + stamina +
                ", defense=" + defense +
                ", agility=" + agility +
                ", intelligence=" + intelligence +
                ", willpower=" + willpower +
                ", level=" + level +
                ", name='" + name + '\'' +
                '}';
    }
}
