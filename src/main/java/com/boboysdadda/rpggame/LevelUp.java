package com.boboysdadda.rpggame;

/**
 * Program:     com.boboysdadda.rpggame
 * File Name:
 * Author:      james
 * Date Created:4/23/2016
 * Description:
 */
public class LevelUp {
    private Dice oneSix = new Dice(1,6);
    private Dice twoSix = new Dice(2,6);
    private int skillPoints;


    public int smallLevel (){
        return oneSix.diceRoller();
    }
    public int mediumLevel(){
        return twoSix.diceRoller();
    }

    public int skillPointRoll (int roll){
        return roll;
    }

}
