package com.boboysdadda.rpggame;

import com.boboysdadda.corelib.UserInputs;

/**
 * Program:     com.boboysdadda.rpggame
 * File Name:   Game.java
 * Author:      james
 * Date Created:4/23/2016
 * Description: A Game that will play similar to DnD
 */
public class Game {

    public static void main(String[] args) {
        UserInputs input = new UserInputs();
        CharacterChoice cc = new CharacterChoice();
        LevelUp lvlUp = new LevelUp();
        int rollCount = 0;
        int skillPoints;
        int intChoice;
        boolean boolChoice;
        String[] classes = {"Warrior",
                "Rouge"};
        String[] attributes = {
                "Strength",
                "Stamina",
                "Defense",
                "Agility",
                "Intelligence",
                "Willpower"
        };

        System.out.println("Please select a class");
        Character player = cc.character(input.getIntOption(classes));
        player.setName(input.getString("Please enter a name for your " + player.characterClass() +
                ": "));
        System.out.println(player);
        System.out.println("You may now add to your default attributes");
        System.out.println("Your default attributes are \n" + player.getAttributes());

        skillPoints = lvlUp.skillPointRoll(lvlUp.mediumLevel());
        System.out.println("You rolled " + skillPoints + " skill points");
        while (rollCount < 2) {
            boolChoice = input.getBooleanOption("If you don't like your roll you may try " +
                    "again 2 " +
                    "more times");
            if (boolChoice) {
                skillPoints = lvlUp.skillPointRoll(lvlUp.mediumLevel());
                System.out.println("You rolled " + skillPoints + " skill points");
                rollCount++;
            }else break;

        }
        while (skillPoints > 0){
            System.out.println("You have "+ skillPoints + " skill points");
            System.out.println("Please assign your skill points");
            System.out.println("Which skill would you like to assign");
            intChoice = input.getIntOption(attributes);
            System.out.println("How many points would you like to assign to this attribute? ");
            int qty = input.getIntOption(skillPoints);
            player.addAttribute(intChoice -1,qty);
            skillPoints -= qty;
            System.out.println(player.getAttributes());
        }

    }

}