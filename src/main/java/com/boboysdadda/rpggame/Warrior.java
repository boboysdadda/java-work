package com.boboysdadda.rpggame;

import java.util.Arrays;

/**
 * Program:     com.boboysdadda.rpggame
 * File Name:
 * Author:      james
 * Date Created:4/23/2016
 * Description:
 */
public class Warrior extends Character {
    private int[] attributes = {7, 5, 4, 2, 2, 1};

    public Warrior() {
        super.setStrength(attributes[0]);
        super.setStamina(attributes[1]);
        super.setDefense(attributes[2]);
        super.setAgility(attributes[3]);
        super.setIntelligence(attributes[4]);
        super.setWillpower(attributes[5]);
    }




    @Override
    public int getStrength() {
        return super.getStrength();
    }

    @Override
    public int getStamina() {
        return super.getStamina();
    }

    @Override
    public int getDefense() {
        return super.getDefense();
    }

    @Override
    public int getAgility() {
        return super.getAgility();
    }

    @Override
    public int getIntelligence() {
        return super.getIntelligence();
    }

    @Override
    public int getWillpower() {
        return super.getWillpower();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public String characterClass() {
        return "Warrior";
    }



    @Override
    public String toString() {
        return "Warrior{" +
                "attributes=" + Arrays.toString(attributes) +
                "} " + super.toString();
    }
}
