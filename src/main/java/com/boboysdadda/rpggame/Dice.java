package com.boboysdadda.rpggame;

import java.util.Arrays;
import java.util.Random;

/**Program:     com.boboysdadda.rpggame
 * File Name:   Dice.java
 * Author:      James
 * Date Created:4/23/2016
 * Description: Dice class to handle random rolls of any number of dice of any number of sides.
 */
public class Dice {

    private int dice;
    private int[] sides;
    private int total;

    public Dice(int qty, int sides) {
        this.sides = new int[sides];

        for (int i = 0; i < this.sides.length; i++) {
            this.sides[i] = i + 1;
        }
        dice = qty;
    }

    public int diceRoller() {
        Random roll = new Random();
        total = 0;
        for (int i = 0; i < dice; i++) {
            int value = roll.nextInt(sides.length);
            total += sides[value];
            System.out.println("value: " + sides[value]);
            System.out.println("total: " + total);

        }
        return total;
    }


    @Override
    public String toString() {
        return "Dice{" +
                "dice=" + dice +
                ", sides=" + Arrays.toString(sides) +
                ", total=" + total +
                '}';
    }
}
