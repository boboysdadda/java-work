package com.boboysdadda.lab2apr2016;

import com.boboysdadda.corelib.FileWorker;
import com.boboysdadda.corelib.MethodSuite;

import java.io.File;
import java.io.IOException;


/**
 * Project:     ${PACKAGE_NAME}
 * FileName:    ${FILE_NAME}
 * Author:      James Dreier
 * Date:        4/2/2016
 * Description: Take a file of strings cast them into an array and then print the array in reverse.
 */
public class lab2apr2016 {
    public static void main(String[] args) throws IOException {
        //the file name
        String fileName = "users";
        //object to work with file
        FileWorker worker = new FileWorker();
        //create the file object
        File file = worker.getTextFile(fileName);
        //Create an array from the file
        String[] test = worker.castTxtFileToStringArray(file);

        //print the array in reverse
        for (int i = test.length; i > 0; i--) {
            System.out.println(test[i - 1]);
        }
        //print array even index

        MethodSuite methodWorker = new MethodSuite();
        int[] IntArray = {1,2,3,4,5,6,7,8,9};
        methodWorker.printEvenIndexs(IntArray);



    }
}
