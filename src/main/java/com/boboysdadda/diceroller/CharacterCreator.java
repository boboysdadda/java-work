package com.boboysdadda.diceroller;

import com.boboysdadda.corelib.UserInputs;

/**
 * Created by james on 4/12/2016.
 */
public class CharacterCreator {
    private UserInputs input = new UserInputs();
    private String[] classChoice = {"Warrior", "Rouge", "Mage", "Ninja", "Healer"};
    private int choice;
    private String charName;


    public int creator() {

        choice = input.getIntOption(classChoice);

        return choice;
    }
    public String nameCharacter() {
        charName = input.getString("Please enter a name for your" + classChoice[choice - 1]);
        return charName;
    }
}

