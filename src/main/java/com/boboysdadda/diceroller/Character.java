package com.boboysdadda.diceroller;

/**Program:     diceroller
 * File Name:   Attributes.java
 * Author:      james
 * Date Created:4/12/2016
 * Description:
 */
class Attributes {
    private int strength;
    private int stamina;
    private int defense;
    private int agility;
    private int intelligence;
    private int willpower;
    private int[] attributes;

    public int getStrength() {
        return strength;
    }

    public int setStrength(int strength) {
        this.strength = strength;
        return strength;
    }

    public int getStamina() {
        return stamina;
    }

    public int setStamina(int stamina) {
        this.stamina = stamina;
        return stamina;
    }

    public int getDefense() {
        return defense;
    }

    public int setDefense(int defense) {
        this.defense = defense;
        return defense;
    }

    public int getAgility() {
        return agility;
    }

    public int setAgility(int agility) {
        this.agility = agility;
        return agility;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public int setIntelligence(int intelligence) {
        this.intelligence = intelligence;
        return intelligence;
    }

    public int getWillpower() {
        return willpower;
    }

    public int setWillpower(int willpower) {
        this.willpower = willpower;
        return willpower;
    }

    void setAttributes(int[] attributes) {

        strength = attributes[0];
        stamina = attributes[1];
        defense = attributes[2];
        agility = attributes[3];
        intelligence = attributes[4];
        willpower = attributes[5];
        this.attributes = attributes;
    }

    int[] getAttributes() {
        return attributes;
    }

    @Override
    public String toString() {
        return "Attributes{" +
                "strength=" + strength +
                ", stamina=" + stamina +
                ", defense=" + defense +
                ", agility=" + agility +
                ", intelligence=" + intelligence +
                ", willpower=" + willpower +
                '}';
    }
}
