package com.boboysdadda.diceroller;

import java.util.Arrays;

/**
 * Created by james on 4/12/2016.
 */
public class WarriorClass extends Character {


    int[] warriorBase = new int[6];



    public void setWarriorBase() {
        this.warriorBase[0] = 7;
        this.warriorBase[1] = 3;
        this.warriorBase[2] = 5;
        this.warriorBase[3] = 2;
        this.warriorBase[4] = 3;
        this.warriorBase[5] = 2;
        setAttributes(warriorBase);
    }



    @Override
    public String toString() {
        return "WarriorClass{" +
                "warriorBase=" + Arrays.toString(warriorBase) +
                '}';
    }
}
