package com.boboysdadda.course;

/**
 * Created by Student1 on 4/9/2016.
 */
class Course {
    private int capacity;
    private int studentsRegistered;
    private String courseNumber;


    int getCapacity() {
        return capacity;
    }

    void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    int getStudentRegistered() {
        return studentsRegistered;
    }

    void setStudentRegistered(int studentsRegistered) {
        if (studentsRegistered + this.studentsRegistered <= capacity) {
            this.studentsRegistered += studentsRegistered;
        }
    }

    String getCourseNumber() {
        return courseNumber;
    }

    void setCourseNumber(String courseNumber) {
        this.courseNumber = courseNumber;
    }

    private boolean isOpen() {
        return capacity > studentsRegistered;
    }

    private int seatsAvailable() {
        return capacity - studentsRegistered;
    }

    void registerStudent() {
        if (isOpen())
            studentsRegistered += 1;
    }

    void dropStudent() {
        studentsRegistered -= 1;
    }

    @Override
    public String toString() {
        return "Course{" +
                "capacity=" + capacity +
                ", studentsRegistered=" + studentsRegistered +
                ", courseNumber='" + courseNumber + '\'' +
                ", seatsAvailable=" + seatsAvailable() +
                '}';
    }
}
