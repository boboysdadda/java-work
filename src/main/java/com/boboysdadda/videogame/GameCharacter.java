package com.boboysdadda.videogame;

import java.util.Arrays;
import java.util.Objects;

/**
 * Program: VideoGame
 * FileName: GameCharacter.java
 * Author: James Dreier
 * Date: 22 April 2016
 * Description: Class to create the characters of a game and store their values.
 **/

class GameCharacter {

    //Create all of the variables
    private String name;
    private int lives;
    private int coins;
    private int friendsCounter = 0;
    //Initialize the friends array
    private GameCharacter[] friends = new GameCharacter[3];
    private boolean lastMushroom = false;
    private boolean isAlive;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null) {
            this.name = name;
        }
    }

    int getLives() {
        return lives;
    }

    public int getCoins() {
        return coins;
    }

    public GameCharacter[] getFriends() {
        return friends;
    }

    void addFriends(GameCharacter friend) {
        if (friend != null) {
            //Make sure there are room for more friends
            if (friendsCounter < this.friends.length) {
                //make sure the friend has more coins than the player
                if (friend.coins > this.coins) {
                    this.friends[friendsCounter] = friend;
                    friendsCounter += 1;
                }
            }
        }
    }

    void collectCoins(int coins) {
        this.coins += coins;
        if (this.coins >= 100) {
            lives++;
            this.coins -= 100;
        }
    }

    boolean isAlive() {
        if (!this.isAlive) {
            return false;
        } else {
            int i = 0;
            this.isAlive = i < lives;
        }

        return this.isAlive;
    }

    void eatMushroom(String color) {

        if (color != null) {
            if (Objects.equals(color, "green") && isAlive()) {
                ++lives;
                lastMushroom = false;
            } else if (Objects.equals(color, "purple") && isAlive()) {
                lives--;
                if (!lastMushroom) {
                    lastMushroom = true;
                } else {
                    isAlive = false;

                }

            }
        }
    }

    void reset() {
        lives = 3;
        coins = 0;
        isAlive = true;
        lastMushroom = false;
    }

    @Override
    public String toString() {
        return "GameCharacter{" +
                "name='" + name + '\'' +
                ", lives=" + lives +
                ", coins=" + coins +
                ", friendsCounter=" + friendsCounter +
                ", friends=" + Arrays.toString(friends) +
                ", lastMushroom=" + lastMushroom +
                ", isAlive=" + isAlive +
                '}';
    }
}
