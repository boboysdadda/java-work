package com.boboysdadda.videogame;

/**
 * Program: VideoGame
 * FileName: VideoGame.java
 * Author: James Dreier
 * Date: 22 April 2016
 * Description: Test the GameCharacter class
 **/
public class VideoGame {
    public static void main(String[] args) {

        //initialize the main player
        GameCharacter player = new GameCharacter();
        player.reset();
        System.out.println(player.getLives());
        player.setName("Toad");

        //lets kill him to make sure purple mushrooms work
        System.out.println(player.isAlive());
        player.eatMushroom("purple");
        System.out.println(player.isAlive());
        player.eatMushroom("purple");
        System.out.println(player.isAlive());
        System.out.println(player.getLives());

        //lets reset the game and make sure the 1up for 100 coins works
        player.reset();
        player.collectCoins(25);
        System.out.println(player);
        player.collectCoins(100);
        System.out.println(player);

        //check to make sure the 1up mushroom works
        player.eatMushroom("green");
        System.out.println(player);

        //create and initialize friends
        //Mario should be in the array
        GameCharacter player2 = new GameCharacter();
        player2.reset();
        player2.setName("Mario");
        player2.collectCoins(80);

        //Luigi shouldn't make it into the friends array
        GameCharacter player3 = new GameCharacter();
        player3.reset();
        player3.setName("Luigi");

        //Princess and Yoshi should
        GameCharacter player4 = new GameCharacter();
        player4.reset();
        player4.setName("Princess");
        player4.collectCoins(70);
        GameCharacter player5 = new GameCharacter();
        player5.reset();
        player5.setName("Yoshi");
        player5.collectCoins(99);

        //Bowser shouldn't. He's a jerk and there shouldn't be any more room.
        GameCharacter player6 = new GameCharacter();
        player6.reset();
        player6.setName("Bowser");
        player6.collectCoins(85);

        //attempt to add all the players to friends
        player.addFriends(player2);
        player.addFriends(player3);
        player.addFriends(player4);
        player.addFriends(player5);
        player.addFriends(player6);

        //print out the results
        System.out.println(player);


    }
}
