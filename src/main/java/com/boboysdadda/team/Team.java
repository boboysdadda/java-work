package com.boboysdadda.team;

/**Program:     team
 * File Name:   Team.java
 * Author:      james
 * Date Created:4/17/2016
 * Description:  Creates a serializable class of Team to arrange players and track teams.
 */

import java.util.Arrays;
import java.io.Serializable;

class Team implements Serializable {
    private int playerCount = 0;
    private String coachName;
    private NBAPlayer[] playerNames;
    private int teamSize = 5;
    private String teamName;

    Team(String teamName){
        playerCount = 0;
        playerNames = new NBAPlayer[teamSize];
        this.teamName = teamName;

    }

    public int getPlayerCount() {
        return playerCount;
    }

    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }

    public void setPlayerNames(NBAPlayer[] playerNames) {
        this.playerNames = playerNames;
    }

    public int getTeamSize() {
        return teamSize;
    }

    public void setTeamSize(int teamSize) {
        this.teamSize = teamSize;
    }

    Team() {
        playerCount = 0;
        playerNames = new NBAPlayer[teamSize];
    }

    public void setCoachName(String coachName) {
        this.coachName = coachName;
    }

    public String getCoachName() {
        return coachName;
    }

    void addPlayerName(NBAPlayer playerName) {

        playerNames[playerCount] = playerName;
        playerCount++;
    }

    public NBAPlayer[] getPlayerNames() {
        return playerNames;
    }

    @Override
    public String toString() {
        return "Team{" +
                "playerCount=" + playerCount +
                ", coachName='" + coachName + '\'' +
                ", playerNames=" + Arrays.toString(playerNames) +
                ", teamSize=" + teamSize +
                ", teamName='" + teamName + '\'' +
                '}';
    }
}