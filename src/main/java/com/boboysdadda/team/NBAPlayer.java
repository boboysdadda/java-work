package com.boboysdadda.team;
/**Program:     team
 * File Name:   NBAPlayer.java
 * Author:      james
 * Date Created:4/17/2016
 * Description:  A class to handle the players
 */

import java.io.Serializable;

class NBAPlayer implements Serializable {
    private String name;
    private String position;
    private int number;

    NBAPlayer(String name, int number, String position) {
        this.name = name;
        this.number = number;
        this.position = position;

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "NBAPlayer{" +
                "name='" + name + '\'' +
                ", position='" + position + '\'' +
                ", number=" + number +
                '}';
    }
}