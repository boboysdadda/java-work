package com.boboysdadda.team;

/**Project Name:    team
 * File Name:       Player.java
 *Author:           James Dreier on 4/16/2016.
 *Description:      Class to handle the basic attributes of a player
 */
public class Player {
    private int jerseyNumber;
    private String name;
    private String position;

    public Player(int jerseyNumber, String name, String position){
        this.jerseyNumber = jerseyNumber;
        this.name = name;
        this.position = position;
    }

    public int getJerseyNumber() {
        return jerseyNumber;
    }

    public void setJerseyNumber(int jerseyNumber) {
        this.jerseyNumber = jerseyNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Player{" +
                "jerseyNumber=" + jerseyNumber +
                ",\n name='" + name + '\'' +
                ",\n position='" + position + '\'' +
                '}'+ "\n";
    }
}
