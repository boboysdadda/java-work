package com.boboysdadda.team;
/**Program:     team
 * File Name:   TeamDriver.java
 * Author:      james
 * Date Created:4/17/2016
 * Description: A driver to run the Team and NBAPlayer class. It utilizes the FileWorker class to
 * write and read the objects from file.
 */

import com.boboysdadda.corelib.FileWorker;
import java.io.*;
import java.util.Arrays;

class TeamDriver {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //creates a worker object to work through the file
        FileWorker worker = new FileWorker();

        //This line reads from the file. If the file doesn't exist then comment out this line and
        // uncomment the block that creates the file.
        Team[] teams = (Team[]) worker.fileToObject("myTeams");

        //This block creates an array of teams and then fills each team with players. It will
        // then write the array to file.

        /*Team[] teams = {new Team("Bulls"), new Team("Clippers"), new Team("Super Sonics")};
        teams[0].setCoachName("Coach1");
        teams[0].addPlayerName(new NBAPlayer("Paul", 15, "pos1"));
        teams[0].addPlayerName(new NBAPlayer("Rob", 16, "pos2"));
        teams[0].addPlayerName(new NBAPlayer("james", 14, "pos3"));
        teams[0].addPlayerName(new NBAPlayer("test1", 13, "pos4"));
        teams[0].addPlayerName(new NBAPlayer("test2", 11, "pos5"));

        teams[1].setCoachName("Coach2");
        teams[1].addPlayerName(new NBAPlayer("player1", 15, "pos6"));
        teams[1].addPlayerName(new NBAPlayer("player2", 16, "pos7"));
        teams[1].addPlayerName(new NBAPlayer("player3", 14, "pos8"));
        teams[1].addPlayerName(new NBAPlayer("player4", 13, "pos9"));
        teams[1].addPlayerName(new NBAPlayer("player5", 11, "pos10"));

        teams[2].setCoachName("Coach3");
        teams[2].addPlayerName(new NBAPlayer("player6", 15, "pos11"));
        teams[2].addPlayerName(new NBAPlayer("player7", 16, "pos12"));
        teams[2].addPlayerName(new NBAPlayer("player8", 14, "pos13"));
        teams[2].addPlayerName(new NBAPlayer("player9", 13, "pos14"));
        teams[2].addPlayerName(new NBAPlayer("player10", 11, "pos15"));

        worker.objectToFile("myTeams", teams);
*/

        System.out.println(Arrays.toString(teams));
    }
}