package com.boboysdadda.userdriver;

/**
 * Program User Driver
 * FileName UserDriver.java
 * Author James Dreier
 * Date 12 APR 2016
 * Description Test the User class
 */
public class User {
    private String lastName;
    private String firstName;
    private int age = 14;
    private String usrName;
    private String pass;

    /**
     * @return Users Last Name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the Users Last Name
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return The Users First Name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName Set the users First Name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return The Users Age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age Set The Users Age
     */
    public void setAge(int age) {
        if (age >= 14 && age < 105) {
            this.age = age;
        }
    }

    /**
     * @return Users userName
     */
    public String getUsrName() {
        return usrName;
    }


    /**
     * @param usrName Sets the Users userName
     */
    public void setUsrName(String usrName) {
        this.usrName = usrName;
    }

    /**
     * @return Users Password
     */
    public String getPass() {
        return pass;
    }

    /**
     * @param pass Set users password
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * Test for valid Password
     *
     * @return boolean if password is valid
     */
    public boolean hasValidPassword() {

        return this.pass != null && !(this.pass.contains("(") || this.pass.contains(")")) && this.pass.length() > 5 && this.pass.length() < 11;
    }

    @Override
    /**
     * Get String of all fields
     */
    public String toString() {
        return "User{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", age=" + age +
                ", usrName='" + usrName + '\'' +
                ", pass='" + pass + '\'' +
                '}';
    }
}
