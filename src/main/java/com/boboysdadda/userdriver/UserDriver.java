package com.boboysdadda.userdriver;

/**
 * Program User Driver
 * FileName UserDriver.java
 * Author James Dreier
 * Date 12 APR 2016
 * Description Test the User class
 */
public class UserDriver {
    public static void main(String[] args) {
        // Initialize the object
        User myUser = new User();

        //Set the variables of the object
        myUser.setLastName("Lovelace");
        myUser.setFirstName("Ada");
        myUser.setUsrName("adalovelace");
        myUser.setPass("test");


        //test everything

        if (!myUser.hasValidPassword()) {
            System.out.println("Invalid Password Null");
        }

        myUser.setPass("test");
        if (!myUser.hasValidPassword()) {
            System.out.println("Invalid Password 1 ");
        }

        myUser.setPass("(test12345");
        if (!myUser.hasValidPassword()) {
            System.out.println("Invalid Password 2 ");
        }

        myUser.setPass("test)12345");
        if (!myUser.hasValidPassword()) {
            System.out.println("Invalid Password 3");
        }

        myUser.setPass("P@55w0rd");
        if (!myUser.hasValidPassword()) {
            System.out.println("Invalid Password 4");
        }

        System.out.println("Your Name is: " + myUser.getFirstName() + " " + myUser.getLastName());

        System.out.println("Login: " + myUser.getUsrName());
        System.out.println("User age: " + myUser.getAge()); //default value is 14

        myUser.setAge(110);
        System.out.println("User age: " + myUser.getAge()); //still prints 14, 110

        myUser.setAge(104);
        System.out.println("User age: " + myUser.getAge());

        System.out.println(myUser.toString());


    }
}
