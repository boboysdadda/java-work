package com.boboysdadda.examStudy;

import com.boboysdadda.corelib.MethodSuite;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Project:     Core-Lib
 * FileName:    ${FILE_NAME}
 * Author:      james
 * Date:        4/1/2016
 * Description:
 */

public class ExamStudy {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     * @throws FileNotFoundException the file not found exception
     */
    public static void main(String[] args) throws FileNotFoundException {
        MethodSuite tester = new MethodSuite();

        //Continue will skip remaining statements in the for loop and start the next iteration of the for loop again.
        for(int i = 0; i < 5; i++) {
            if(i == 4) {
                continue;
            }
            System.out.print(i + " ");
        }
        System.out.println();
        //break will end the loop so when i == 4 the print statement is skipped because the loop has ended.
        for(int i = 0; i < 5; i++) {
            if(i == 4) {
                break;
            }
            System.out.print(i + " ");
        }
        System.out.println();
        //write a method addEvens that takes an array of integers and returns the sum of the even numbers.
        int[] numbArray = {1,2,3,4,5,6,7,8,9,10};
        System.out.println(tester.addEvens(numbArray));

        //write a method reverseArray that takes in an array of strings and prints them backwards.
        String[] stringArray = {"a","b","c","d","e"};
        tester.reverseArray(stringArray);

        Scanner fileReader = new Scanner(new File("myFile.txt"));
        String line;

        //This will store a line, skip a line, then print the stored line, then repeat.
        while(fileReader.hasNext()) {
            line = fileReader.nextLine();
            if(fileReader.hasNext()){
                fileReader.nextLine();
            }
            System.out.println(line);

        }

        System.out.println(combine(5,6));

        String[] array = new String[]{"a","b","c"};
        for (String anArray : array) {
            System.out.println(anArray);
        }


    }
    public static String combine(int a, int b){
        return a +"" + b;
    }
}

