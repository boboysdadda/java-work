package com.boboysdadda.cartester;

/**
 * Created by Student1 on 4/9/2016.
 */
public class CarTester {
    public static void main(String[] args) {
        Car car = new Car();

        car.setYear(1982);
        car.setMake("Ford");
        car.setModel("Escort");
        car.setMileage(28000);
        car.setLastOilChangeMileage(25680);

        System.out.println("Year: " + car.getYear());
        System.out.println("Make:  " + car.getMake());
        System.out.println("Model " + car.getModel());

        System.out.println("Current mileage: " + car.getMileage());
        System.out.println("Miles since Last oil change: " + car.sinceLastOilChange());

        car.drive();
        System.out.println("Current mileage" + car.getMileage());
        System.out.println("Miles since Last oil change: " + car.sinceLastOilChange());

        car.drive(45);
        System.out.println("Current mileage" + car.getMileage());
        System.out.println("Miles since Last oil change: " + car.sinceLastOilChange());

        System.out.println("Oil change is due: " + car.oilChangeDue() );


    }
}
