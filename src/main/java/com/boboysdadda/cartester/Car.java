package com.boboysdadda.cartester;

/**
 * Created by Student1 on 4/9/2016.
 */
class Car {


    private String make;
    private String model;
    private int year;
    private int mileage;
    private int lastOilChangeMileage;


    String getModel() {
        return model;
    }

    void setModel(String model) {
        this.model = model;
    }

    String getMake() {
        return make;
    }

    void setMake(String make) {
        this.make = make;
    }

    int getYear() {
        return year;
    }

    void setYear(int year) {
        this.year = year;
    }

    int getMileage() {
        return mileage;
    }

    void setMileage(int mileage) {
        this.mileage = mileage;
    }

    int getLastOilChangeMileage() {
        return lastOilChangeMileage;
    }

    void setLastOilChangeMileage(int lastOilChangeMileage) {
        this.lastOilChangeMileage = lastOilChangeMileage;
    }

    void drive() {
        mileage += 1;
    }

    void drive(int milesDriven) {
        mileage += milesDriven;
    }

    int sinceLastOilChange() {
        return mileage - lastOilChangeMileage;
    }

    boolean oilChangeDue() {
        int oilChangeMileage = 3000;
        return sinceLastOilChange() >= oilChangeMileage;

    }


}
