package com.boboysdadda.GrepProgram;


import com.boboysdadda.corelib.FileWorker;
import com.boboysdadda.corelib.UserInputs;

import java.io.File;
import java.io.IOException;

/**
 * Project:     GrepProgram
 * FileName:    GrepProgram.java
 * Author:      james
 * Date:        3/31/2016
 * Description:
 */
public class GrepProgram {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     * @throws IOException the io exception
     */
    public static void main(String[] args) throws IOException {
        //create objects
        FileWorker file = new FileWorker();
        UserInputs input = new UserInputs();

        //get file name
        String fileName;
        fileName = input.getString("Please enter a file name");

        //create file
        File workingFile = file.getTextFile(fileName);

        //request user input
        String search = input.getString("What would you like to search for");

        //output line of occurrences.
        file.getLineNumber(workingFile, search);


    }
}
