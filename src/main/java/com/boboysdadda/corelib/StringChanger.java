package com.boboysdadda.corelib;

/**
 * Program: User Account Manager
 * Filename: StringChanger.java
 * Author: James Dreier
 * Date: 19Mar2016
 * Description: A set of methods to manipulate Strings
 */
public class StringChanger {
    /**
     * Password hash string.
     *
     * @param password String that you want hashed
     * @return Takes the password and then iterates through the String and casts the char into an int value. then adds 1 to that int value. Then it casts that int value back into a char and adds it to the hashCode String.
     */
    public static String passwordHash(String password) {
        String hashCode = "";
        for (int i = 0; i < password.length(); i++) {
            int intValue = (int) password.charAt(i);
            intValue++;
            hashCode += (char) intValue;
        }
        return hashCode;
    }

    /**
     * String combine string.
     *
     * @param a The username you want used
     * @param b The hashed password you want
     * @return the concatenated username and password in the format a:b
     */
    public static String stringCombine(String a, String b) {
        return a + ":" + b;
    }

    /**
     * Gen pass from hash string.
     *
     * @param hashCode The hash of the password you want decrypted
     * @return the password the user entered originally
     */
    public static String genPassFromHash(String hashCode) {
        String password = "";
        for (int i = 0; i < hashCode.length(); i++) {
            int intValue = (int) hashCode.charAt(i);
            intValue--;
            password += (char) intValue;
        }
        return password;

    }

}
