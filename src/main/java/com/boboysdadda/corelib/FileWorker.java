package com.boboysdadda.corelib;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Vector;

/**
 * Program: Grep Program
 * FileName: FileWorker.java
 * Author: James Dreier
 * Reference Source: http://www.vogella.com/tutorials/JavaIO/article.html
 * Date: 24 Mar 2016
 * Description: A set of methods for creating, reading, and writing files.
 */


public class FileWorker {
    /**
     * Read text file string.
     *
     * @param fileName the file you want to work with
     * @return a string from the file you are reading
     * @throws IOException the io exception
     */
    public String readTextFile(File fileName) throws IOException {
        return new String(Files.readAllBytes(Paths.get(fileName.toString())));
    }

    /**
     * Reads the file line by line by using a List of Strings
     *
     * @param fileName the file you want to work with
     * @return a String for each line in the file into a list
     * @throws IOException the io exception
     */
    private List<String> readTextFileByLines(File fileName) throws IOException {
        return Files.readAllLines(Paths.get(fileName.toString()));
    }

    /**
     * Write to the file
     *
     * @param fileName the file you want to work with
     * @param content  what you want added to the file
     * @throws IOException the io exception
     */
    public void writeToTextFile(File fileName, String content) throws IOException {
        System.out.println();
        content += "\n";
        Files.write(Paths.get(fileName.toString()), content.getBytes(), StandardOpenOption.SYNC, StandardOpenOption.APPEND);
    }

    /**
     * Create the file if it hasn't been created already
     *
     * @param fileName the file you want created
     * @throws IOException
     */
    private void createTxtFile(String fileName) throws IOException {
        File file = new File(fileName + ".txt");
        if (file.createNewFile()) {
            System.out.println("file created");
        } else System.out.println("file already exists");
    }

    /**
     * get the file so that it can be worked with
     *
     * @param fileName The name of the file you want to work with
     * @return the file
     * @throws IOException the io exception
     */
    public File getTextFile(String fileName) throws IOException {
        File file = new File(fileName + ".txt");
        if (!file.exists()) {
            createTxtFile(fileName);
        }
        return file;
    }

    /**
     * Check if user and pass exists in a file
     *
     * @param fileName The Name of the file you want to work with
     * @param userPass The User Pass hash you want to verify
     * @return boolean value
     * @throws IOException the io exception
     */
    public boolean userPassExists(File fileName, String userPass) throws IOException {
        boolean userPassExistsCheck = false;

        List line = readTextFileByLines(fileName);
        if (line.contains(userPass)) {
            userPassExistsCheck = true;
        }

        return userPassExistsCheck;
    }

    /**
     * Test if User already exists before requesting password
     *
     * @param fileName The password file you want to iterate through
     * @param userName The username you want to test for
     * @return boolean true = username exists
     * @throws IOException the io exception
     */
    public boolean userExists(File fileName, String userName) throws IOException {
        boolean userExists = false;

        List list = readTextFileByLines(fileName);
        int i = 0;
        while (i < list.size()) {
            String s = list.get(i).toString();
            int l = s.indexOf(':');

            if (userName.equals(s.substring(0, l))) {
                userExists = true;
                break;
            } else {
                i++;
            }
        }
        return userExists;
    }

    /**
     * Cast a file containing one integer per line to an int array
     *
     * @param fileName The name of the file you want to work with
     * @return an int array
     * @throws IOException the io exception
     */
    public int[] castTxtFileToIntArray(File fileName) throws IOException {
        List list = readTextFileByLines(fileName);
        int i;
        int[] c = new int[list.size()];
        for (i = 0; i < list.size(); i++) {
            String a = list.get(i).toString();
            c[i] = Integer.parseInt(a);
        }

        return c;
    }

    /**
     * Create a String array from file
     *
     * @param fileName the filename you want to work with
     * @return a String array
     * @throws IOException
     */
    public String[] castTxtFileToStringArray(File fileName) throws IOException {
        List list = readTextFileByLines(fileName);
        int i;
        String[] c = new String[list.size()];
        for (i = 0; i < list.size(); i++) {
            c[i] = list.get(i).toString();
        }
        return c;
    }

    /**
     * Gets line number.
     *
     * @param fileName The name of the file you want to work with
     * @param a        The String you want to search for
     * @return int value representing the line it is found on starting at 0 being the first line
     * @throws IOException the io exception
     */
    public int getLineNumber(File fileName, String a) throws IOException {
        List list = readTextFileByLines(fileName);
        int i;
        boolean found = false;
        for (i = 0; i < list.size(); i++) {
            String s = list.get(i).toString();
            if (s.contains(a)) {
                System.out.println("line: " + (i + 1) + s);
                found = true;
            }
        }
        if (!found) {
            System.out.println("String not found");
            System.exit(0);
        }
        return i;
    }

    /**
     * Gets line number.
     *
     * @param fileName the file name
     * @param a        the a
     * @return the line number
     * @throws IOException the io exception
     */
    public int getLineNumber(File fileName, int a) throws IOException {
        List list = readTextFileByLines(fileName);
        int i;
        boolean found = false;
        for (i = 0; i < list.size(); i++) {
            String s = list.get(i).toString();
            if (Integer.parseInt(s) == a) {
                System.out.println("line: " + (i + 1));
                found = true;
            }
        }
        if (!found) {
            System.out.println("String not found");
            System.exit(0);

        }

        return i;
    }

    /**
     * Saves an Object to a data file
     * Use of this method requires that the object class
     * that you wish to save be serializable. You must import java.io.Serializable and then
     * implement it into your object class.
     *
     * @param fileName   The name you want to call the file. (.data will be appended to the end)
     * @param objectName The Object you want saved
     * @throws IOException
     */
    public void objectToFile(String fileName, Object objectName) throws IOException {
        // Write to disk with FileOutputStream
        FileOutputStream fOut = new FileOutputStream(fileName + ".data");
        // Write object with ObjectOutputStream
        ObjectOutputStream objOut = new ObjectOutputStream(fOut);
        // Write object out to disk
        objOut.writeObject(objectName);
    }

    /**
     * Retrieves an object from the data file
     * Use of this method requires that the object class
     * that you wish to save be serializable. You must import java.io.Serializable and then
     * implement it into your object class.
     *
     * @param fileName The name of the file you want to work with. data will be appended to the end)
     * @return The object you want. You must cast the object to the one you are working with.
     * Example: Team bulls = (Team) worker.fileToObject("myTeam");
     */
    public Object fileToObject(String fileName) throws IOException {

        // Read from disk using FileInputStream
        // Read object using ObjectInputStream
        try (FileInputStream f_in = new FileInputStream(fileName + ".data"); ObjectInputStream
                obj_in = new ObjectInputStream(f_in)) {
            // Read  and return an object
            return obj_in.readObject();

        } catch (ClassNotFoundException e) {
            System.out.println("Trouble Reading from file" + e.getMessage());

        }
        return null;
    }
}





