package com.boboysdadda.corelib;

/**
 * Created by Student1 on 4/9/2016.
 */
public class Person {

    //data
    private String name;
    private int age;

    //methods
    //Note: for every property, lets add
    //  1: getter method - to retrieve the value of the property
    //  2: setter method - to set the value of the property


    /**
     * Gets the value of the name
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name
     * @param name the name you want to set.
     */
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}