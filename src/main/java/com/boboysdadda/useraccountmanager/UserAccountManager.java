package com.boboysdadda.useraccountmanager;

import com.boboysdadda.corelib.FileWorker;
import com.boboysdadda.corelib.StringChanger;
import com.boboysdadda.corelib.UserInputs;

import java.io.File;
import java.io.IOException;

/**
 * Program: User Account Manager
 * Version: 2.1
 * FileName: UserAccountManager.java
 * Author: James Dreier
 * Date Created: 24 Mar 2016
 * Description: Create and verify user accounts and passwords for authentication to a system.
 */
public class UserAccountManager {

    /**
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

        String userFileName = "passwords";
        FileWorker worker = new FileWorker();
        UserInputs input = new UserInputs();

        File testFile = worker.getTextFile(userFileName);
        String[] options = {"Create new user",
                "Login to system"};
        String userRequest = "Please enter your use name: ";

        /**
         * try and catch to prevent error messages for someone entering anything other than an integer.
         */

        int option = input.getIntOption(options);
        String username,
                password,
                userPass;
        switch (option) {
            /**
             * Case 1 is used for creating the account and verifying that the username does not exist and then
             * ensuring the password meets the complexity requirements
             */
            case 1:
                /**
                 * Requests username and then goes through a do while loop to verify that the username is not
                 * currently in use
                 */
                username = input.getString(userRequest);

                /**
                 * cannot create line if the file does not have a line to crete from. Check to make sure the
                 * file has a line to work with before verifying the username. If there is no line then the
                 * username is automatically valid.
                 */

                boolean userExists = worker.userExists(testFile, username);
                if (userExists) {
                    System.out.println("Username: " + username + " already exists. Please choose another one");
                    System.exit(0);
                }

                password = input.getString("Please enter a password between 8 - 15 characters: ");
                boolean passwordCheck = input.passwordCheck(password);
                if (!passwordCheck) {
                    System.out.println("Password is not valid");
                    System.exit(0);
                }
                userPass = StringChanger.stringCombine(username, StringChanger.passwordHash(password));
                /**
                 * writes the userPass to the password file then closes the file.
                 */
                worker.writeToTextFile(testFile, userPass);

                System.out.println("Your account has been created.  You can now log in to the system.");
                break;
            /**
             * case 2 is used for logging in the user and verifying that the user pass combo is acceptable.
             */
            case 2:
                username = input.getString(userRequest);
                password = input.getString("Please enter your password");
                userPass = StringChanger.stringCombine(username, StringChanger.passwordHash(password));
                /**
                 * this will iterate through the userPass in passwords.txt and verify that there is a match.
                 */

                boolean userPassExists = worker.userPassExists(testFile, userPass);
                if (userPassExists) {
                    System.out.println("Welcome to the system");
                    break;
                } else System.out.println("Invalid Login");
                break;
            default:
                System.out.println("You must select from options 1 and 2.");
        }

    }
}